#hisoftWHMS
##前言
+ 本系统为自动签到系统，暂时只支持海辉工时系统。
+ 后续会开通百度贴吧、11对战平台的自动签到功能。
+ 有其他自动签到需求的童鞋，请在issues区留言。

##功能区
###海辉工时系统
+ 每日定时登陆工时系统，填写当天工时。
+ 工作日填写 __出勤__ ，周末填写 __调休__ ，若属于节假日，则也会录入 __调休__ 。

>有问题，请发送邮件：[657439380@qq.com](mailto:657439380@qq.com)