package com.romin.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HisoftWorkTypeUtil {

    /**
     * 获取当天的工作类型
     * @return
     */
    public static HisoftWorkType getCurWorkType() {
        return getDateWorkType(new Date());

    }

    /**
     * 根据指定的日期返回工作类型
     * @param date
     * @return
     */
    public static HisoftWorkType getDateWorkType(Date date) {
        Calendar cale = Calendar.getInstance();
        cale.setTime(date);
        int curWeek = cale.get(Calendar.DAY_OF_WEEK);
        if (curWeek == Calendar.SATURDAY || curWeek == Calendar.SUNDAY) {
            return HisoftWorkType.TIAO_XIU;
        }
        return HisoftWorkType.CHU_QIN;
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(HisoftWorkTypeUtil.getDateWorkType(format.parse("2014-2-21")).getName());
    }

    public enum HisoftWorkType {
        CHU_QIN("945d0e58-7dd7-470e-93e5-119c333b543b", "出勤"), CHU_CHAI("db0c98ae-e744-4b9b-919b-d72287d34044", "出差"), QING_JIA(
                "9a5c417e-35f9-4953-ad87-ac4185cf02bf", "请假"), TIAO_XIU("083abe6d-66b3-4e7e-9d6b-12a99dafd259", "调休"),
        KUANG_GONG("1d22d1f0-15be-425f-a345-0fac9c02d03a", "旷工");

        private final String name;
        private final String value;

        private HisoftWorkType(String value, String name) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
