package com.romin.task;

import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class HelloAnnotationTest {

    @Scheduled(cron = "0/10 * * * * ?")
    public void excute() {
        System.out.println("excute------" + new Date());
    }

}
