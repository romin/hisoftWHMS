package com.romin.task;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.romin.service.AcountService;

/**
 * 每日凌晨初始化登陆信息，将关联的登陆表中，设置sign_flag字段为未签到'0'
 * @author romin
 *
 */
public class InitSignInfoTask implements Task {
    private static final Logger logger = LoggerFactory.getLogger(InitSignInfoTask.class);
    @Resource
    private AcountService acountService;
    @Override
    public void execute() {
        logger.info("init the user sign info.");
        acountService.initSignInfo();
    }
}
