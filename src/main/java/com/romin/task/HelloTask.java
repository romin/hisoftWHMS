package com.romin.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloTask implements Task {

    private final static Logger logger = LoggerFactory.getLogger(HelloTask.class);
    @Override
    public void execute() {
        System.out.println("hello world");
        logger.info("[INFO]:hello luozm");
        logger.error("[ERROR]:hello chunling");
    }

}
