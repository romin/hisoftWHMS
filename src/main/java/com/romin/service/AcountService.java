package com.romin.service;

import com.romin.model.LoginUserBean;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AcountService {
    private static final Logger logger = LoggerFactory.getLogger(AcountService.class);
    @Resource
    private DataSource dataSource;

    /**
     * 获取未签到的信息bean
     * @return
     */
    public List getLoginBean() {
        String sql = "select id, bind_user 'userName',bind_pwd 'password',type_id 'bindType' from bind_acount t where t.type_id=? and t.status='1' and t.sign_flag='0'";
        List res = null;
        try {
             res= (List)new QueryRunner(dataSource).query(sql, new BeanListHandler(LoginUserBean.class), "1");
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        if (res == null)
            res = new ArrayList();
        return res;
    }
    
    /**
     * 用户签到成功
     * @param id
     * @return
     */
    public int signAcount(int id) {
        String sql = "update bind_acount t set t.sign_flag='1' where t.status='1' and t.id=?";
        int flag = 0;
        try {
            flag = new QueryRunner(dataSource).update(sql, id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 初始化签到标志位，每天早上1点执行初始化操作
     * @return
     */
    public int initSignInfo() {
        String sql = "update bind_acount t set t.sign_flag='0' where t.status='1'";
        int flag = 0;
        try {
            flag = new QueryRunner(dataSource).update(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
}
