package com.romin.action;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.romin.task.HisoftHourTask;

@Controller
@RequestMapping("/cron")
public class CronMainExcuteAction {
    @Resource
    public HisoftHourTask hisoftHourTask;

    @RequestMapping("/hisoftWorkHour")
    public String excute(Model model) {
        hisoftHourTask.execute();
        model.addAttribute("result", "SUCCESS");
        return "cronRes";
    }
}
