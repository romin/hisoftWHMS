package com.romin.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/hello")
public class HelloAction {
    private final Logger logger = LoggerFactory.getLogger(HelloAction.class);
    @RequestMapping("/aaa")
    public String hello(Model model) {
        model.addAttribute("myname", getAAAName());
        logger.error(getAAAName());
        return "hello";
    }

    @RequestMapping("/bbb")
    public String bbb(Model model)
    {
        return "hello";
    }
    public String getHerName() {
        return "luozmaa";
    }

    public String getYourName() {
        return "maxiaofeng";
    }

    public String getAAAName() {
        return "xiaoboaaaaaaaaaaaaaaaaaa hehehehe";
    }

    public void testHttpClient() {

    }
}
