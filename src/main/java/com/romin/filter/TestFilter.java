package com.romin.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by romin on 2014/5/20.
 */
@javax.servlet.annotation.WebFilter(filterName = "TestFilter" ,urlPatterns = "/*")
public class TestFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        System.out.println("begin");
        chain.doFilter(req, resp);
        System.out.println("after");
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
