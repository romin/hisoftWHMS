package com.romin.test.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.romin.service.AcountService;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class AcountServiceTest extends AbstractJUnit4SpringContextTests {

    @Resource
    private AcountService acountService;
    @Test
    public void testName() throws Exception {
        System.out.println("excute:init--------start");
        acountService.initSignInfo();

        System.out.println("excute:init--------end");
    }

    @Test
    public void testSing() throws Exception {
        System.out.println("excute:sign--------start");
        acountService.signAcount(1);
        System.out.println("excute:sign--------end");
    }

    @Test
    public void testContext() throws Exception {
        System.out.println(this.applicationContext);
        System.out.println(this.applicationContext.getId());
        System.out.println(this.applicationContext.getDisplayName());

    }
}
