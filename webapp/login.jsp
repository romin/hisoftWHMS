<%@page contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>登陆页面login</title>
	<script type="text/javascri>pt" src="./res/js/jquery/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="./res/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="./res/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="./res/css/signin.css">
</head>
<body>
    <div class="container">
      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">用户登入：</h2>
        <input type="text" class="form-control" placeholder="Email address" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登陆</button>
      </form>
    </div> <!-- /container -->
</body>
</html>