<%--
  Created by IntelliJ IDEA.
  User: romin
  Date: 2014/5/16
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>jquery-validate测试</title>
    <script src="../res/js/jquery/jquery-1.7.2.min.js"></script>
    <script src="../res/js/jquery-validate/1.11.1/jquery.validate.min.js"></script>
    <script src="../res/js/jquery-validate/1.11.1/messages_bs_zh.js"></script>
    <link rel="stylesheet" href="../res/js/jquery-validate/1.11.1/validate.css"/>
    <style type="text/css">
        dt {width:100px;}
        dt,dd {float: left;}
        dl {clear: both;}

    </style>
    <script type="application/javascript">

        $(function(){
                $("#myForm").validate({
                    debug:true,
                    rules: {
                        username: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                        required: true
                        }
                    },
                    messages:{
                        username:"请输入姓名",
                        email:{
                            required:"请输入邮箱地址"
                        }
                    }
                });

        });

    </script>
</head>
<body>
<form action="#" method="post" id="myForm">
    <dl>
        <dt><label>名称：</label></dt>
        <dd><input type="text" name="username" id="username" /></dd>
    </dl>

    <dl>
        <dt><label>密码：</label></dt>
        <dd><input type="text" name="password" id="password"/></dd>
    </dl>
    <dl>
        <dt><label>邮箱地址:</label></dt>
        <dd><input type="text" name="email" id="email"/></dd>
    </dl>
    <input type="submit" value="提交"/>
</form>
</body>
</html>
