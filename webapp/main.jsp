<%@page contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>登陆页面</title>
	<script type="text/javascript" src="./res/js/jquery/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="./res/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="./res/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="./res/js/main.js"></script>
</head>
<body>
<nav class="navbar navbar-default " role="navigation">
	<div class="container" >
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="http://www.baidu.com">网站主页</a>
	</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">个人中心</a></li>
			<li><a href="#">绑定账号</a></li>
			<li><a href="#">网站功能</a></li>
			<li><a href="#">关于</a></li>
		</ul>
	</div><!-- /.navbar-collapse -->
	</div>
</nav>
<div class="container">
	<div class="alert alert-info">
		该页面为亚信联创技术资源合作部外包管理系统签到自动化页面。
	</div>
	<div class="alert alert-success">
		<p/>工作类型自动识别，正常工作日均为出勤，周末以及公休日均为调休。
		<p/>该自动化签到程序每天早上6点进行签到，工作类型暂时仅支持出勤与调休，工作内容对应为出勤或者调休。
		<p/>请假的请在当日早上6点之后重新编辑工作类型与工作内容，暂时只能手动进行请假签到。
	</div> 

	<button class="button btn-primary btn-lg">新增</button>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>绑定类型</th>
				<th>用户名</th>
				<th>密码</th>
				<th>启用</th>
				<th>当日签到</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>亚联外包工时系统</td>
				<td>a65743898</td>
				<td><a hfer="javascript:" onclick="alert('alert');">******</a></td>
				<td><button type="button" class="btn btn-success autoState btn-xs">已启用</button></td>
				<td><span class="label label-success signState">已签到</span></td>
				<td>
					<button type="button" class="btn btn-danger btn-sm">删除</button>&nbsp;
					<button type="button" class="btn btn-danger btn-sm" data-target="#myModal" data-toggle="modal">编辑</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">编辑</h4>
      </div>
      <div class="modal-body">

        	<form action="" method="POST" role="form">
        		<div class="form-group">
        			<label for="">类型：</label>
        			<select name="" id="input" class="form-control" disabled="disabled">
        				<option value="">亚联外包签到系统</option>
        				<option value="">-- Select Two --</option>
        			</select>
        		</div>
        		<div class="form-group">
        			<label for="">用户名：</label>
        			<input type="text" class="form-control" id="" placeholder="">
        		</div>
        		<div class="form-group">
        			<label for="">密码：</label>
        			<input type="text" class="form-control" id="" placeholder="">
        		</div>
        	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="button" class="btn btn-primary">提交</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>